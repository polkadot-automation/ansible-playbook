# Polkadot Playbook

This repo have Ansible playbook that can manage to deploy and update Polkadot full node.



## Requirements
1. The IPs of the nodes that will run Polkadot (will be visible when you use terraform-live code in the final output of the execution)
2. The private key to access the instances (Since its a sensitive value it won't appear at the end of terraform execution, you have to explicitly execute the command `terraform output private_key_pem` to get it).

## Procedure

Leveraging the Ansible role by Paritytech owner of Polkadot implementation, the following are the steps to kick off the deployment:

1. Since the used AMI is Ubuntu, AWS tends to name the user ubuntu, so we set Ansible user, the path to private key file we get from Terraform and disable host checking for a smooth Ansible run.
```shell
export ANSIBLE_REMOTE_USER=ubuntu
export ANSIBLE_PRIVATE_KEY_FILE=<the path to the private key>
export ANSIBLE_HOST_KEY_CHECKING=False
```
2.  Update the inventory file with nodes IPs you get from Terraform run output.

```yaml
polkadot_nodes:
  hosts:
    node1:
      ansible_host: X.X.X.X
    node2:
      ansible_host: X.X.X.X
```
3.  Pull the role that will deploy Polkadot using the requirements.yaml file
```shell
 ansible-galaxy collection install -f -r requirements.yml -p ./collections
```
4. Execute the playbook using the following command
``` shell
ansible-playbook -i inventory/ deploy-fullnode-polkadot.yml -vv --diff
```
Now Polkadot service should available in the nodes, you should be able to run `systemctl status polkadot` and see that the service is up and running.

### Upgrade to v0.9.41
Upgrades are necessary for patching old issues or bringing up a new features, sometimes things can go wrong drastically if the upgrade was not planned and analyzed.
A good starting point would be the changelog and the upgrade recommendation from the maintainers and regarding upgrading Polkadot from **v0.9.39-1**	to **v0.9.41** they should be no breaking changes, however, just to be on the safe side it's better to upgrade first to  **v0.9.40**  and then we go ahead to **v0.9.41**.
We will need to update the version variable in the playbook.
```yaml
node_binary_version: v0.9.41
```
and then we will need to execute the playbook with tag `node-binary` 
```shell
ansible-playbook -i inventory/ deploy-fullnode-polkadot.yml -vv --diff --tags node-binary
```